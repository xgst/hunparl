"""
init
"""
"""
from .letolto import scraper
from .letolto import szam_lista
from .letolto import legujabb_szam
from .konvertalo import pdf_to_txt
from .tisztazo import ogy_n_tisztazo
from .parser import szam
from .parser import ciklus
from .parser import ules_datum
from .parser import elnok_lista
from .parser import jegyzo_lista
from .parser import torzs_szoveg
from .parser import torveny_javaslat_lista
from .parser import hatarozati_javaslat_lista
from .parser import kepviselo_lista
from .parser import kepviseloi_felszolalas_szotar
from .parser import reakcio_lista
"""
from hunparl import letolto
from hunparl import konvertalo
from hunparl import tisztazo
from hunparl import parser